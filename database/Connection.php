<?php

class Connection{


	//STATIC FUNCTION: je moet geen object aanmaken om gebruik te maken van deze functie
	public static function make(){
		//try maak en return pdo object and catch -> probeer, indien niet gelukt -> actie
		try{
			$pdo = new PDO('mysql:host=127.0.0.1:8889;dbname=car_shop','root','root');
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
		}
		catch (PDOException $e){
			die($e->getMessage());
		}
		
	}
}