<?php
session_start();



if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

if (isset($_GET['removeId'])) {
    $removeId = $_GET['removeId'];
}

if (isset($_GET['cancelOrder'])) {
    $cancelOrder = $_GET['cancelOrder'];
    session_destroy();
}

if (isset($_GET['value'])) {
    $value = $_GET['value'];
}

// add to cart
if (!empty($id) && empty($value)) {
    $cart = new Cart;
    //car oproepen met dit id
    $cart->addtocart($id);
    redirect('/car/order');
}

if (!empty($value)) {
    $cartV = new Cart;
    $cartV->updatecart($id,$value);
    redirect('/car/order');
}


if (!empty($removeId)) {
    $cartR = new Cart;
    $cartR->removefromcart($removeId);
    redirect('/car/order');
}

if (!empty($cancelOrder)) {

    $cartD = new Cart;
    $cartD->cancelOrder($cancelOrder);
    redirect('/cars');
}

if (!empty($_POST)) {
    redirect('car/order/save');
}


$arrayMetAlleGegevens = array();
if (!empty($_SESSION['cart'])) {
    foreach ($_SESSION['cart'] as $id=>$amount):
        $allInfo = $query->selectwithid('cars', $id);
        $allInfo->amount = $amount;
        array_push($arrayMetAlleGegevens, $allInfo);
    endforeach;
}
$arrayMetAlleGegevens = array_reverse($arrayMetAlleGegevens);

$totalPrice_sum = 0;
$totalAmount_sum = 0;
foreach ($arrayMetAlleGegevens as $total):
$allprice = ($total->prijs * (1+ $total->btwtarief) * $total->amount);
$totalPrice_sum = $totalPrice_sum + $allprice;
$totalAmount_sum = $totalAmount_sum + $total->amount;
$_SESSION['totalAmount_sum'] = $totalAmount_sum;
endforeach;

if ($totalAmount_sum == 0) {
    unset($_SESSION['totalAmount_sum']);
}

if(isset($_SESSION['discountPrice'])) {
    $_SESSION['totalDiscount_sum'] = $totalPrice_sum - $_SESSION['discountPrice'];
    if( $_SESSION['totalDiscount_sum'] < 0 ){
        $_SESSION['totalDiscount_sum'] = 0;
    }
}

require 'views/includes/start.inc.php';

require_once('views/car_order.view.php');

require 'views/includes/end.inc.php';