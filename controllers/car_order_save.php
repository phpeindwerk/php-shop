<?php
session_start();

unset($_SESSION['cart']);
unset($_SESSION['totalAmount_sum']);
$arrayMetAlleGegevens = '';
if (isset($_SESSION['promoX'])) {
    $kortingsCode =  $_SESSION['promoX'];

    $discount = $query->select('kortings', 'code', "$kortingsCode")[0];

    $update = $query->update('kortings',
        ['status' => 1],
        $discount->id);

    unset($_SESSION['promoUpdate']);
    unset($_SESSION['promoX']);
    unset($_SESSION['discountPrice']);
    unset($_SESSION['notValid']);

}

require 'views/includes/start.inc.php';

require_once('views/car_order_save.view.php');

require 'views/includes/end.inc.php';