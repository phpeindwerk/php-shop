Eindwerk deel 1
Nu we een router en een querybuilder hebben gemaakt, is het tijd om deze handige classes te gaan gebruiken. 



Herwerk de phpshop van de eerste lessen in een nieuw project waarbij je gebruik maakt van de router, de querybuilder, controllers en views.



Vereisten



Maak een class Product met volgende eigenschappen



properties:

id

naam

omschrijving

prijs

btwtarief

beoordeling (uitbreiding)

aantalbeoordelingen (uitbreiding)



methods:

toonprijsinclbtw

beoordelen (uitbreiding)



Voorzie volgende functionaliteiten in je shop:

- product lijst (bvb raster)

- product toevoegen

- product verwijderen

- product detailpagina (product?id=XX)

- product bewerken



Maak hierbij steeds gebruik van een controller en een view. Zorg voor de juiste request method (POST/GET).

Let op: controller verwerkt het verzoek (logica) en de view presenteert de data indien nodig.



Breid je viewbestanden uit met de nodige includes

Voorzie bootstrap in je layout. (breid de views uit met een aantal partial views - htmltop, htmlbottom)



Uitbreiding

Voorzie de mogelijkheid om een product te beoordelen. Doe dit alsvolgt

Voorzie een aantal sterren die worden weergegeven langs mekaar. Elke ster stelt een getal voor tussen 1 en 5.

Als op de eerste ster geklikt wordt, gaat de browser naar product/beoordeel?product=ID&score=1.

De controller voor deze pagina verwerkt deze score door het aantal beoordelingen op te hogen en een gemiddelde te maken van alle scores. 

Geef deze score weer op je productdetailpagina. 