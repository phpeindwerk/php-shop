<?php 

$router->get('','controllers/index.php');
$router->get('cars','controllers/cars.php');
$router->get('cars/muscle','controllers/muscle_cars.php');
$router->get('cars/family','controllers/family_cars.php');
$router->get('car/detail','controllers/car_detail.php');
$router->get('car/add','controllers/car_add.php');
$router->get('car/edit','controllers/car_edit.php');
$router->get('car/delete','controllers/car_delete.php');
$router->get('car/order','controllers/car_order.php');
$router->post('car/save','controllers/car_save.php');
$router->post('car/order/save','controllers/car_order_save.php');
//$router->get('car/order/save','controllers/car_order_save.php');
$router->post('car/edit/save','controllers/car_edit_save.php');
$router->post('ratestars/save','controllers/ratestars_save.php');
$router->post('car/discount','controllers/car_discount.php');
$router->get('car/discount','controllers/car_discount.php');


//dd($router);