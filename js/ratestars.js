var f = document.getElementById('ratestars');
f.style.display = 'none';

function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e.style.display === 'none')
        e.style.display = 'block';
    else
        e.style.display = 'none';
}