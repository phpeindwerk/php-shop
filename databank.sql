CREATE DATABASE car_shop;
USE car_shop;

CREATE TABLE categories (
	id INT AUTO_INCREMENT PRIMARY KEY,
	naam VARCHAR(255) NOT NULL
);

INSERT INTO categories (naam) VALUES
('muscle cars'),
('family cars');

CREATE TABLE cars (
  id INT PRIMARY KEY AUTO_INCREMENT,
  naam VARCHAR(99) NOT NULL,
  korteomschrijving TINYTEXT,
  omschrijving TEXT,
  image VARCHAR(255) NOT NULL,
  stock INT,
  prijs FLOAT,
  btwtarief FLOAT,
  beoordeling tinyint(5),
  aantalbeoordeling INT,
  categories_id INT(10)

);

INSERT INTO cars (naam, korteomschrijving, omschrijving, image, stock, prijs, btwtarief, beoordeling, aantalbeoordeling, categories_id) VALUES
('Challenger-sxt', 'Uconnect 4 includes a 7-Inch Touchscreen, Android Auto, Apple CarPlay Support, integrated Uconnect Voice Command...', 'Uconnect 4 includes a 7-Inch Touchscreen, Android Auto, Apple CarPlay Support, integrated Uconnect Voice Command, Bluetooth Streaming Audio and SiriusXM Satellite Radio, Dual-Zone Automatic Temperature Control ,305-HP 3.6L Pentastar V6 Engine','/images/challenger-sxt.jpeg', 10, 26995, 0.21, 4, 1, 1),
('Charger', 'ParkView Rear Back Up Camera and ParkSense Rear Park Assist System, SiriusXM Satellite Radio. Available LED...', 'ParkView Rear Back Up Camera and ParkSense Rear Park Assist System, SiriusXM Satellite Radio. Available LED Fog Lamps. Available 707-HP Supercharged 6.2L HEMI SRT Hellcat V8 Engine. Five-Link Independent Rear Suspension. TorqueFlite Eight-Speed Automatic Transmission. Available Full-Speed Forward Collision Warning with Active Braking.','/images/charger2018.jpeg', 10, 28495, 0.21, 4, 1, 1),
('Durango', '18-Inch Satin Carbon Aluminum Wheels. Maximum Towing Capacity of 6,200 Pounds. 3.6L Pentastar V6 Engine...', '18-Inch Satin Carbon Aluminum Wheels. Maximum Towing Capacity of 6,200 Pounds. 3.6L Pentastar V6 Engine. ParkView Rear Back Up Camera', '/images/durango.jpeg', 25, 29995, 0.21, 4, 1, 2),
('Grand Caravan', 'Six-Speaker Audio System. Third-Row Stow ''n Go Seating and Storage System Featuring Class-Exclusive Tailgate Seating...', "Six-Speaker Audio System. Third-Row Stow 'n Go Seating and Storage System Featuring Class-Exclusive Tailgate Seating. Body-Color Door Handles and Side Moldings.", '/images/grand-caravan.jpeg', 14, 25995, 0.21, 4, 1, 2),
('Journey-se', 'Both FWD and AWD. Models Have a Highway Driving Range of Over 500 Miles Per Tank. 2.4L Four-Cylinder Engine...', 'Both FWD and AWD. Models Have a Highway Driving Range of Over 500 Miles Per Tank. 2.4L Four-Cylinder Engine or 3.6L Pentastar V6 Engine (FWD/AWD). Seven-Passenger Seating with the Standard Third-Row Seat. ','/images/journey-se.jpeg', 5, 22495, 0.21, 4, 1, 2);




CREATE TABLE users (
	id INT AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL
);

INSERT INTO users ( username, password) VALUES ('admin', 'secret');




CREATE TABLE kortings (
  id INT AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(10),
  status tinyint(1) DEFAULT 0,
  discount INT(10)
);

INSERT INTO kortings (code, status, discount) VALUES
('lk201812', 0, 1000),
('zk201834', 0, 1000),
('hk201856', 0, 1000),
('wk201878', 0, 1000);














