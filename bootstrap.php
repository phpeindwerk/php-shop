<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('database/Connection.php');
require_once('inc/functions.inc.php');
require_once('database/Querybuilder.php');
require_once('classes/Categorie.php');
require_once('classes/Car.php');
require_once('classes/Cart.php');
require_once('classes/Korting.php');
require_once('core/Router.php');
require_once('core/Request.php');

$pdo = Connection::make();
$query = new Querybuilder($pdo);


// OF $query = new Querybuilder(Connection::make());
