<h1>Edit Car</h1>
<hr>

<form action="/car/edit/save" method="post" class="edit-form">
    <div class="row">
        <div class="col-md-6 space-between">
            <input type="hidden" name="hiddenId" value="<?= $car->id; ?>">
            <label for="naam">Name of the car</label>
            <input type="text" class="form-control" id="naam" name="naam" value="<?= $car->naam; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 space-between">
            <label for="prijs">Price of the car
                <small>no tax.</small>
            </label>
            <input type="text" class="form-control" id="prijs" name="prijs" value="<?= $car->prijs; ?>">
        </div>
        <div class="col-md-2 space-between">
            <label for="btwtarief">Tax</label>
            <input type="text" class="form-control" id="btwtarief" name="btwtarief" value="<?= $car->btwtarief; ?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 space-between">
            <label for="image">Image</label>
            <input type="text" class="form-control" id="image" name="image" value="<?= $car->image; ?>">
        </div>
        <div class="col-md-2 space-between">
            <label for="stock">Stock</label>
            <input type="text" class="form-control" id="stock" name="stock" value="<?= $car->stock; ?>">
        </div>
    </div>
    <div class="row">
        <legend class="col-form-label col-sm-2 pt-0 edit-legend">Category</legend>
        <div class="col-sm-10">
            <select name="Cnaam" id="Cnaam" class="form-control-sm edit-categorie">
                <?php foreach ($categories as $categorie): ?>
                    <option value="<?= $categorie->id; ?>">

                        <?= $categorie->naam; ?>
                    </option>

                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="omschrijving">Discription</label>
                <textarea class="form-control" rows="5" id="omschrijving"
                          name="omschrijving"><?= $car->omschrijving; ?></textarea>
            </div>
        </div>
    </div>
    <br>
    <input type="submit" name="addcar" class="btn btn-primary edit-submit" value="Save">
</form>