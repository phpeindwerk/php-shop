</div>
</div>

</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a class="top" href="#">Back to top</a>
        </p>
        <p class="lead text-foot">Created for PHP OOP</p>
        <p class="lead text-foot">
            by <a class="site-link" href="https://frankmooren.be" target="_blank">Frank Mooren</a>,
               <a class="site-link" href="https://nimor.be" target="_blank">Nicky Morre</a>
            and
               <a class="site-link" href="https://kimvanrijckel.be" target="_blank">Kim Vanrijckel</a>
        </p>
    </div>
</footer>
</body>
</html>