<h1>Add Car</h1>
<hr>

<form action="/car/edit" method="post" class="add-form">
    <div class="row">
        <div class="col-md-6 name">
            <label for="naam">Name of the car</label>
            <input type="text" class="form-control" id="naam" name="naam">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 price space-between">
            <label for="prijs">Price of the car
                <small>no tax.</small>
            </label>
            <input type="text" class="form-control" id="prijs" name="prijs">
        </div>
        <div class="col-md-2 space-between">
            <label for="btwtarief">Tax</label>
            <input type="text" class="form-control" id="btwtarief" name="btwtarief">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 space-between">
            <label for="image">Image</label>
            <input type="text" class="form-control" id="image" name="image">
        </div>
        <div class="col-md-2 space-between">
            <label for="stock">Stock</label>
            <input type="text" class="form-control" id="stock" name="stock">
        </div>
    </div>
    <div class="row">
        <legend class="col-form-label col-sm-2 pt-0 add-legend">Category</legend>
        <div class="col-sm-10 add-categorie">
            <select name="Cnaam" id="Cnaam" class="form-control-sm">

                <?php foreach ($categories as $categorie): ?>
                    <option value="<?= $categorie->id; ?>">

                        <?= $categorie->naam; ?>
                    </option>

                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="omschrijving">Discription</label>
                <textarea class="form-control" rows="5" id="omschrijving" name="omschrijving"></textarea>
            </div>
        </div>
    </div>
    <br>
    <input type="submit" name="addcar" class="btn btn-primary add-submit" value="Save">
</form>