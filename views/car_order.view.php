<h1>Order Car</h1>
<hr>


<div class="row">
    <div class="col-md-12 order">
        <h4 class="mb-3"><b>Selected products</b></h4>
        <div class="row">
            <table class="table">
                <thead>
                <tr class="bg-dark">
                    <th scope="col">Car</th>
                    <th scope="col">Price (excl. Tax)</th>
                    <th scope="col">Tax</th>
                    <th scope="col">Price (incl. Tax)</th>
                    <th scope="col">Amount</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arrayMetAlleGegevens as $singleCar): ?>
                    <tr>
                        <th class="ctr" scope="row"><?= $singleCar->naam; ?></th>
                        <td class="cta">$&nbsp;<?= $singleCar->prijs; ?></td>
                        <td class="cta"><?= $singleCar->btwtarief * 100; ?>&nbsp;%</td>
                        <td class="cta">$&nbsp;<?= $singleCar->prijs * (1 + $singleCar->btwtarief); ?></td>
                        <td>
                            <form action="/car/order" method="get">
                                <input class="order-amount" title="amount" style="width:60px;" type="number"
                                       name="value" min="1" value="<?= $singleCar->amount; ?>">
                                <input type="hidden" name="id" value="<?= $singleCar->id; ?>">
                                <button type="submit" class="btn btn-info btn-sm order-refresh"><i
                                            class="fa fa-refresh"></i></button>
                            </form>
                        </td>
                        <td>
                            <a href="/car/order?removeId=<?= $singleCar->id; ?>" title="delete"
                               class="btn btn-danger btn-sm order-delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr class="bg-light">
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">Total price</th>
                    <th scope="col">Total products</th>
                    <th scope="col"></th>
                </tr>

                <tr>
                    <th class="cta"></th>
                    <th class="cta"></th>
                    <th class="cta"></th>
                    <th class="cta">$&nbsp;<?= $totalPrice_sum; ?></th>
                    <th class="cta"><?= $totalAmount_sum; ?></th>
                    <th class="cta"></th>
                </tr>
                <?php if (isset($_SESSION['discountPrice'])): ?>
                    <tr>
                        <th class="cta"></th>
                        <th class="cta"></th>
                        <th class="">Discount :</th>
                        <th class="ctr">-&nbsp;$&nbsp;<?= $_SESSION['discountPrice'] ?></th>

                        <th class="cta"></th>
                        <th class="cta"></th>
                    </tr>
                    <tr>
                        <th class="cta"></th>
                        <th class="cta"></th>
                        <th class=""></th>
                        <th class="cta">$&nbsp;<?= $_SESSION['totalDiscount_sum'] ?></th>

                        <th class="cta"></th>
                        <th class="cta"></th>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <?php if (!isset($_SESSION['discountPrice'])): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <form action="/car/discount" method="post" class="promo">
                            <div class="input-group">
                                <input name="discount" type="text" class="form-control" placeholder="Promo code">
                                <div class="input-group-append">
                                    <input type="submit" class="btn btn-secondary" value="Redeem">
                                </div>
                            </div>
                        </form>
                        <?php if(isset($_SESSION['notValid'])): ?>
                              <p class="ctr">This discount code is not valid!</p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-12 billing">
        <h4 class="mb-3">Billing address</h4>
        <form class="needs-validation order-form" action="/car/order/save" method="post" novalidate>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="firstName">First name</label>
                    <input type="text" class="form-control" id="firstName" placeholder="First name" value="" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName">Last name</label>
                    <input type="text" class="form-control" id="lastName" placeholder="Last name" value="" required>
                </div>
            </div>

            <div class="mb-3">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="carshop@mail.com">
            </div>

            <div class="mb-3">
                <label for="address">Address</label>
                <input type="text" class="form-control" id="address" placeholder="Schachtplein 1, 3550 Heusden-Zolder"
                       required>
            </div>

            <div class="mb-3">
                <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                <input type="text" class="form-control" id="address2" placeholder="Bus or Block">
            </div>

            <div class="row">
                <div class="col-md-5 mb-3">
                    <label for="country">Country</label>
                    <select class="custom-select d-block w-100" id="country" required>
                        <option value="">Choose...</option>
                        <option>Belgium</option>
                        <option>Netherlands</option>
                    </select>
                </div>
            </div>
            <hr class="mb-4">

            <input type="submit" name="ordercar" class="btn btn-primary btn-lg btn-block"
                   value="Continue to checkout">


        </form>
        <div class="row">
            <div class=" col-sm-12">


                <a href="/car/order?cancelOrder=1">
                    <button class="btn btn-danger cancel">Cancel Order</button>
                </a>
            </div>
        </div>

    </div>
</div>