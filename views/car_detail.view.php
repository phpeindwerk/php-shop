<div class="row">
    <div class="col-md-12">
        <div class="jumbotron">
            <div class="main-content">
                <div class="row">
                    <div class="col-md-8 car-head">
                        <h1 class="titelnaam detail"><?= $car->naam; ?></h1>
                        <hr>
                        <img class="car-image detail" src="<?= $car->image; ?>" alt="<?= $car->naam; ?>">
                        <hr>
                    </div>
                    <div class="col-md-4 car-body">
                        <hr>
                        <p class="car-prijs detail"><b>Price (excl. tax): </b><span
                                    class="cta">$<?= $car->prijs; ?></span>
                        </p>
                        <p class="car-btwtarief detail"><b>Tax: </b><span class="cta"><?= $car->btwtarief*100; ?>%</span>
                        </p>
                        <p class="car-volprijs detail"><b>Full price (incl. tax): </b><span
                                    class="cta">$<?= $volledigeprijs; ?></span></p>
                        <p class="car-stock detail"><b>Currently in stock: </b><span
                                    class="cta"><?= $car->stock; ?></span>
                        </p>
                        <h6><b>Description: </b></h6>
                        <p class="car-omschrijving detail"><?= $car->omschrijving; ?></p>
                        <div class="rating detail">
                            <h6><b>Rating</b></h6>

                            <?php require ('views/beoordeling.view.php')

                            ?>

                        </div>
                        <div class="reviews detail">
                            <h6><b>Number of reviews:</b>
                                <small><span class="cta reviews-amount"><?= $car->aantalbeoordeling; ?></span></small>
                            </h6>
                        </div>
                        <div class="btn-group detail">
                            <a href="/car/order?id=<?= $car->id; ?>">
                                <button type="button" class="btn btn-sm btn-success">Order</button>
                            </a>
                        </div>
                        <div class="btn-group customer detail">
                            <button type="button" class="btn btn-sm btn-primary" id="addreviewbtn" onclick="toggle_visibility('ratestars')">Add Rating</button>
                        </div>
                        <div class="btn-group admin detail">
                            <a href="/car/edit?id=<?= $car->id; ?>">
                                <button type="button" class="btn btn-sm btn-warning">Edit</button>
                            </a>
                            <a href="/car/delete?id=<?= $car->id;?>">
                                <button type="button" class="btn btn-sm btn-danger">Delete</button>
                            </a>
                        </div>

                        <form action="/ratestars/save" method="post" class="rating" id="ratestars">

                            <span class="starWrapper">

                                <input type="hidden" value="<?=$car->id ?>" name="hiddenId">
                                <input type="hidden" value="<?=$car->aantalbeoordeling ?>" name="hiddenBeoordeling">


                                <input type="radio" id="star5" name="rating" value="<?=$car->beoordeling + 5 ?>" />
                                <label for="star5" title="Awesome - 5 stars"></label>


                                <input type="radio" id="star4" name="rating" value="<?=$car->beoordeling + 4 ?>" />
                                <label for="star4" title="Pretty good - 4 stars"></label>


                                <input type="radio" id="star3" name="rating" value="<?=$car->beoordeling + 3 ?>" />
                                <label for="star3" title="Meh - 3 stars"></label>


                                <input type="radio" id="star2" name="rating" value="<?=$car->beoordeling + 2 ?>" />
                                <label for="star2" title="Kinda bad - 2 stars"></label>


                                <input type="radio" id="star1" name="rating" value="<?=$car->beoordeling + 1 ?>" />
                                <label for="star1" title="Sucks big time - 1 star"></label>
                            </span>

                            <input type="submit" name="btn" value="Submit your rating" class="btn btn-primary">

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/ratestars.js"></script>