<div class="row">

    <div class="col-sm-12">
        <a href="car/add" class="btn btn-primary add front">Add car</a>

        <div class="btn-group pull-right">
            <button type="button" class="btn btn-info filter front dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter on category
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="/">All cars</a>
                <a class="dropdown-item" href="/cars/muscle">Muscle cars</a>
                <a class="dropdown-item" href="/cars/family">Family cars</a>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <h1 class="cars-list-titel">Our complete offer</h1>
    </div>

    <?php foreach ($cars as $car): ?>

    <div class="col-md-4">
        <div class="card mb-5 box-shadow front">
            <h1 class="titelnaam front"><?= $car->naam; ?></h1>
            <img class="card-img-top front" src="<?= $car->image; ?>" alt="<?= $car->naam; ?>">
            <div class="card-body front">
                <p class="card-prijs front"><b>Price (excl. tax): </b><span class="cta">$<?= $car->prijs; ?></span></p>
                <p class="card-stock front"><b>Currently in stock: </b><span class="cta"><?= $car->stock; ?></span></p>
                <p class="card-text front"><?= substr($car->omschrijving, 0, 100).'....'; ?></p>
                <div class="rating front">
                    <h6><b>Rating</b></h6>

                    <?php require ('views/beoordeling.view.php')
                    ?>
                </div>
                <div class="reviews front">
                    <h6><b>Number of reviews:</b>
                        <small><b><span class="cta reviews-amount"><?= $car->aantalbeoordeling; ?></span></b></small>
                    </h6>
                </div>
                <div class="btn-group front">
                    <a href="/car/detail?id=<?= $car->id; ?>">
                        <button type="button" class="btn btn-sm btn-success">View</button>
                    </a>
                    <a href="/car/order?id=<?= $car->id; ?>">
                        <button type="button" class="btn btn-sm btn-success">Order</button>
                    </a>
                </div>
                <div class="btn-group admin front">
                    <a href="/car/edit?id=<?= $car->id; ?>">
                        <button type="button" class="btn btn-sm btn-warning">Edit</button>
                    </a>
                    <a href="/car/delete?id=<?= $car->id; ?>">
                        <button type="button" class="btn btn-sm btn-danger">Delete</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php endforeach; ?>