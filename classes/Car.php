<?php

class Car{

	public $prijs;
	public $btwtarief;
	public $aantalbeoordeling;
	public $beoordeling;

	public function prijsinclusiefbtw(){
		$incbtwtarief =  $this->prijs*(1+$this->btwtarief);
		return round($incbtwtarief, 2);
	}
	public function calStars(){
	    $gemiddelde = $this->beoordeling/$this->aantalbeoordeling;
	    return round($gemiddelde, 0);
    }
}


