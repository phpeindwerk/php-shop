<?php

class Cart
{
    public $amount = 1;

    public function __construct()
    {
        if (empty($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }
    }

    public function addtocart($id)
    {
        if (isset($_SESSION['cart'][$id])) {
            $_SESSION['cart'][$id]++;

        } else {
            $_SESSION['cart'][$id] = $this->amount;

        }
    }

    public function updatecart($id, $value)
    {

        $_SESSION['cart'][$id] = $value;


    }

    public function removefromcart($removeId)

    {
        unset($_SESSION['cart'][$removeId]);
    }

    public function cancelOrder()
    {
        $_SESSION['cart'] = '';
    }
}
