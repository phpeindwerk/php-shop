<?php

class Router{
	protected $routes = [
		'GET' => [],
		'POST' => []
	];

	public function get($uri,$controller){
		$this->routes['GET'][$uri] = $controller;
	}

	public function post($uri,$controller){
		$this->routes['POST'][$uri] = $controller;
	}

	public function direct($uri,$requesttype){
		if(array_key_exists($uri, $this->routes[$requesttype])){
			return $this->routes[$requesttype][$uri];
		}
		else{
			throw new Exception("Deze route bestaat niet", 1);
			
		}
	}
}